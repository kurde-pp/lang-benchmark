import std.file;
import std.stdio;
import std.conv;
import std.string;
import std.array;
import std.algorithm;

struct pos
{
	uint x;
	uint y;
}

class star
{
	this()
	{
		points = new pos[0];
		points.length = 0;
	}

	uint x;
	uint y;
	pos[] points;

}

class pgm_img
{
	public
	this()
	{
		data = new ushort[1];
	}

	ushort[] data;
	ushort sizex;
	ushort sizey;
	ushort get_pixel(uint x, uint y)
	{
		return data[sizex * y + x];
	}
}

void search_star(ref star st, ref pgm_img img, uint x, uint y, uint threshold)
{
	if (x > 0 && x < img.sizex && y > 0 && y < img.sizey)
	{
		if (img.get_pixel(x, y) >= threshold)
		{
			auto is_there = find(st.points, pos(x, y)).empty;
			if (is_there)
			{
				st.points.length++;
				st.points[$ - 1] = pos(x, y);
				search_star(st, img, x + 1, y, threshold);
				search_star(st, img, x , y - 1, threshold);
				search_star(st, img, x , y + 1, threshold);
				search_star(st, img, x - 1, y , threshold);

			}
		}

	}

}

pgm_img read_pgm(string path)
{
	auto pgm = new pgm_img;
	File file = File(path);

	file.readln();
	file.readln();
	string ssize = file.readln();
	string[] sizes = ssize.split(" ");
	pgm.sizex = to!ushort(sizes[0]);
	pgm.sizey = to!ushort(sizes[1][0 .. $ - 1]);
	file.readln();
	pgm.data.length = pgm.sizex * pgm.sizey;
	for (uint i = 0; i < pgm.sizex * pgm.sizey; i++)
	{
		string line;
		line = file.readln();
		pgm.data[i] = to!ushort(line[0 .. $ - 1]);

	}
	return pgm;
}

void color_star(ref ubyte[] img, ref star st, uint sizex, uint sizey)
{
	for (uint i = 0; i < st.points.length; i++)
	{
		img[(st.points[i].y * sizex + st.points[i].x) * 3] = 0xff;
		img[(st.points[i].y * sizex + st.points[i].x) * 3 +1] = 0x0;
		img[(st.points[i].y * sizex + st.points[i].x) * 3 +2] = 0x0;

	}
}

ubyte[] create_img(ref pgm_img img)
{
	ubyte[] data = new ubyte[img.sizex * img.sizey * 3];
	for (uint i = 0; i < data.length / 3; i++)
	{
		data[i * 3] = (cast(uint) img.data[i]) * 255 / 65535;
		data[i * 3 + 1] = (cast(uint) img.data[i]) * 255 / 65535;
		data[i * 3 + 2] = (cast(uint) img.data[i]) * 255 / 65535;

	}
	return data;
}

void main(string[] args)
{
	star[] stars = new star[0];
	pgm_img pgm_file = read_pgm("gwa.pgm");
	ubyte[] img_out = create_img(pgm_file);
	for (uint y = 0; y < pgm_file.sizey; y++)
	{
		for (uint x = 0; x < pgm_file.sizex; x += 2)
		{

			if (pgm_file.get_pixel(x, y) > 150)
			{
				star tmp = new star;
				tmp.x = x;
				tmp.y = y;
				search_star(tmp, pgm_file, x, y, 150);
				if (pgm_file.get_pixel(x, y) > 400)
				{
					if (tmp.points.length > 2)
					{
						color_star(img_out, tmp, pgm_file.sizex, pgm_file.sizey);
					}
				}
				else
				{
					if (tmp.points.length > 6)
					{
						color_star(img_out, tmp, pgm_file.sizex, pgm_file.sizey);
					}
				}
			}
		}
	}
	File Dout = File("out_d.rgb","wb");
	Dout.rawWrite(img_out);
	writeln("size x:", pgm_file.sizex, " size y:", pgm_file.sizey, " last: ", pgm_file
			.data[$ - 1]);

}
