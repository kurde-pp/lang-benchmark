use std::{
    fs::File,
    io::{Read, Write},
};

struct Pgm {
    x: u32,
    y: u32,
    m: u32,
    data: Vec<u32>,
}
struct Star{
	x:u32,
	y:u32,
	points:Vec<[u32;2]>
}

impl Pgm {
    fn load(path: &str) -> Pgm {
        let mut f = File::open(path).unwrap();
        let mut buf = String::new();
        f.read_to_string(&mut buf).unwrap();
        let mut lines = buf.split("\n");
        lines.next();
        lines.next();
        let mut dims = lines.next().unwrap().split(" ");
        let x = dims.next().unwrap().parse::<u32>().unwrap();
        let y = dims.next().unwrap().parse::<u32>().unwrap();
        let m = lines.next().unwrap().parse::<u32>().unwrap();
        let mut data = vec![];
        for _ in 0..x * y {
            data.push(lines.next().unwrap().parse::<u32>().unwrap())
        }
        Pgm { x, y, m, data }
    }
}
impl Star{
	pub fn new(x :u32 ,y:u32) -> Self{
		Star{
			x:x,
			y:y,
			points : Vec::with_capacity(6)
		}
	}

}
fn find_star(x: u32, y: u32, pgm: &Pgm, st: &mut Star) {
    if (1..pgm.x).contains(&x) && (1..pgm.y).contains(&y) {
        if pgm.data[(x + pgm.x * y) as usize] > 150 {
            if !st.points.contains(&[x, y]) {
                st.points.push([x, y]);
                find_star(x - 1, y, pgm, st);
                find_star(x + 1, y, pgm, st);
                find_star(x, y - 1, pgm, st);
                find_star(x, y + 1, pgm, st);
            }
        }
    }
}
fn color_star(out: &mut Vec<u8>, x: &u32, st: &Star) {
    for p in st.points.iter() {
        out[((p[1] * x + p[0]) * 3) as usize] = 255;
        out[((p[1] * x + p[0]) * 3 + 1) as usize] = 0;
        out[((p[1] * x + p[0]) * 3 + 2) as usize] = 0;

    }
}
fn create_out(pgm: &Pgm) -> Vec<u8> {
    let mut vec = vec![0; (pgm.x * pgm.y * 3) as usize];
    for i in 0..pgm.x * pgm.y {
        vec[(i * 3) as usize] = (pgm.data[i as usize] * 255 / pgm.m) as u8;
        vec[(i * 3 + 1) as usize] = (pgm.data[i as usize] * 255 / pgm.m) as u8;
        vec[(i * 3 + 2) as usize] = (pgm.data[i as usize] * 255 / pgm.m) as u8;

    }
    vec
}
fn main() {
    let pgm = Pgm::load("gwa.pgm");
    let mut out = create_out(&pgm);
    let mut min_size;
    let mut luminace;

    for y in 0..pgm.y {
        for x in (0..pgm.x).step_by(2) {
            luminace = pgm.data[(x + pgm.x * y) as usize];
			
            if luminace > 150 {
				let mut st = Star::new(x,y);
                min_size = 6;
                find_star(x, y, &pgm, &mut st);
                if luminace > 400 {
                    min_size = 2;
                }
                if st.points.len() > min_size {
                    color_star(&mut out, &pgm.x, &st);
                }
            }
        }
    }
    let mut f = File::create("./rust_out.data").unwrap();
    f.write(&out).unwrap();
}
