# Language speed benchmark
 
## Algorithm
 
Loads pgm(portable gray map) file to array. This operation require
converting string to numbers. I next step allocate output image in rgb24 format and copy
pgm data into it. ANd then  that search light points on image. If pixel exceed threshold checks if adjecting pixels
also exceed threshold. If this group is big enough pixels are painted red on output image.
At the end the image is exported to raw data or bmp.


## Launguages
- C++
- D
- C#(by cub8)
- Ruby(by cub8)
- Python
- Zig (by sppmacd)
- C
- Rust (by mesagungan)
- JS(by Aleksander)
- Lua
- Go (planned)
## Help wanted in
- Perl
- PHP
- Elixir
- Erlang
- Java

## Benchmark

Every program is tested on "gwa.pgm" using command hyperfine.
You can use bigger or smaller  test file.  Generate your custom image by
running pgm-generator. 
```bash
cd  pgm-generator
cargo build  --release
cd target/release
./pgm-generator <width> <height> <name.pgm>
mv <name.gwa> ../../../gwa.pgm
```
## Bulding

``./builder.sh``
Builds an benchmarks programs.
Print result to file build/RESULTS.md

Dependecies
- mono
- dotnet 3.1
- zig
- gdc
- c compiler
- c++ compiler
- python3
- ruby
- node 
- npm
- cargo

## Results(28.02.2022)
Linux Manjaro 5.15  Intel I7 8700K
Pgm size 2000x2000 
| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `./CCode` | 179.1 ± 4.1 | 171.3 | 185.4 | 1.13 ± 0.04 |
| `./C++Code` | 158.1 ± 3.8 | 151.6 | 167.1 | 1.00 |
| `./RustCode` | 162.9 ± 6.1 | 156.7 | 184.1 | 1.03 ± 0.05 |
| `./ZigCode` | 439.3 ± 14.7 | 425.2 | 477.2 | 2.78 ± 0.11 |
| `./DCode` | 636.6 ± 55.1 | 577.8 | 714.4 | 4.03 ± 0.36 |
| `mono Stars.dll` | 1114.1 ± 19.6 | 1096.9 | 1155.3 | 7.05 ± 0.21 |
| `./CSharpCode/Stars` | 809.1 ± 29.2 | 774.0 | 844.7 | 5.12 ± 0.22 |
| `node JsCode.js` | 1145.6 ± 55.2 | 1113.3 | 1298.3 | 7.25 ± 0.39 |
| `python3 ../../python/main.py` | 7161.5 ± 67.3 | 7048.9 | 7288.6 | 45.31 ± 1.18 |
| `ruby ../../ruby/gwiazdy.rb` | 13293.0 ± 118.0 | 13125.3 | 13452.1 | 84.09 ± 2.17 |
