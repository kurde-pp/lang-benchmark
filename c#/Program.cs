﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Stars
{
    class Pixel {
        public readonly uint x;
        public readonly uint y;
        public Pixel (uint _x, uint _y) {
            x = _x;
            y = _y;
        }
        public static bool operator ==(Pixel px1, Pixel px2) {
            return px1.x == px2.x && px1.y == px2.y;
        }
        public static bool operator !=(Pixel px1, Pixel px2) {
            return px1.x != px2.x && px1.y != px2.y;
        }
        public override bool Equals(object obj) { 
            Pixel px = obj as Pixel;
            return x == px.x && y == px.y;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
    class RGB {
        public readonly byte r;
        public readonly byte g;
        public readonly byte b;
        public RGB (byte _r, byte _g, byte _b) {
            r = _r;
            g = _g;
            b = _b;
        }
    }

    class PGM {
        public readonly uint x;
        public readonly uint y;
        public uint[] data;
        public PGM(uint _x, uint _y) {
            x = _x;
            y = _y;
            data = new uint[x * y];
        }

        public uint Get_pixel(Pixel pixel) {
            return data[pixel.x + x * pixel.y];
        }
    }

    class PPM {
        public readonly uint x;
        public readonly uint y;
        public RGB[] data;
        public PPM(uint _x, uint _y) {
            x = _x;
            y = _y;
            data = Enumerable.Repeat(new RGB((byte)0, (byte)0, (byte)0), Convert.ToInt32(x * y)).ToArray();
        }

        public void Put_pixel(Pixel pixel, RGB rgb) {
            data[pixel.x + x * pixel.y] = rgb;
        }
    }

    class Star {
        public List<Pixel> pixels;
        public Star()
        {
            pixels = new List<Pixel>();
        }
    }

    class StarsArcheologist {
        private readonly string filepath_in;
        private readonly string filepath_out;
        private readonly PGM image;
        private readonly PPM save_image;
        public StarsArcheologist(string _filepath) {
            filepath_in = _filepath;
            filepath_out = "gwa.data";
            // filepath_out = filepath_in.Replace("pgm", "ppm");
            image = Load_img();
            save_image = new PPM(image.x, image.y);

        }
        public void Run() {
            uint min_size;
            for (uint y = 0; y < image.y; y++) { 
                for (uint x = 0; x < image.x; x += 2) {
                    Pixel current_pixel = new Pixel(x, y);
                    if (image.Get_pixel(current_pixel) > 150) {
                        if (image.Get_pixel(current_pixel) > 400)
                            min_size = 2;
                        else
                            min_size = 6;

                        Star star = new Star();
                        Find_star(current_pixel, star, 150);

                        if (star.pixels.Count > min_size) {
                            Color_star(star);
                        }
                    }
                }
            }
            // Write_img();
            Write_raw_data();

        }
        private PGM Load_img() {
            StreamReader file = new StreamReader(filepath_in);
            file.BaseStream.Seek(0, SeekOrigin.Begin);
            string rl;
            rl = file.ReadLine();
            rl = file.ReadLine();
            uint x, y;
            rl = file.ReadLine();
            uint[] xy = Array.ConvertAll(rl.Split(' '), s => uint.Parse(s));
            x = xy[0];
            y = xy[1];
            PGM _image = new PGM(x, y);
            rl = file.ReadLine();
            for (int _y = 0; _y < y; _y++) { 
                for (int _x = 0; _x < x; _x++) {
                    rl = file.ReadLine();
                    _image.data[_x + x * _y] = Convert.ToUInt32(rl);
                }
            }
            file.Close();

            return _image;
        }
        private void Find_star(Pixel pixel, Star star, uint thershold) {
            if (pixel.x > 0 && pixel.y > 0 && pixel.x < image.x && pixel.y < image.y) { 
                if (image.Get_pixel(pixel) > thershold) { 
                    if (!star.pixels.Contains(pixel)) {
                        star.pixels.Add(pixel);
                        Find_star(new Pixel(pixel.x - 1, pixel.y), star, thershold);
                        Find_star(new Pixel(pixel.x + 1, pixel.y), star, thershold);
                        Find_star(new Pixel(pixel.x, pixel.y - 1), star, thershold);
                        Find_star(new Pixel(pixel.x, pixel.y + 1), star, thershold);
                    }
                }
            }
        }
        private void Color_star(Star star) {
            foreach(Pixel px in star.pixels) {
                save_image.data[px.x + save_image.x * px.y] = new RGB((byte)255, (byte)0, (byte)0);
            }
        }
        private void Write_raw_data() {
            BinaryWriter file = new BinaryWriter(File.OpenWrite(filepath_out));
            foreach (RGB rgb in save_image.data) {
                file.Write(rgb.r);
                file.Write(rgb.g);
                file.Write(rgb.b);
            }
            file.Flush();
            file.Close();
        }
        // private void Write_img() {
        //     StreamWriter file = new StreamWriter(filepath_out);
        //     file.WriteLine("P3");
        //     file.WriteLine($"{save_image.x} {save_image.y}");
        //     file.WriteLine("255");
        //     foreach (RGB rgb in save_image.data) {
        //         file.WriteLine(rgb.r);
        //         file.WriteLine(rgb.g);
        //         file.WriteLine(rgb.b);
        //     }
        // }

    }

    class Program {
        static void Main(string[] args) {
            StarsArcheologist archeologist = new StarsArcheologist("gwa.pgm");
            archeologist.Run();
        }
    }
}
