import PIL
import PIL.Image


class pgm:
	def __init__(self):
		self.x = 0
		self.y = 0
		self.m = 65535
		self.data = []

	def getpixel(self, x: int, y: int):
		return self.data[x + self.x * y]


class star:
	def __init__(self):
		self.pixels = []
		self.x = 0
		self.y = 0


def load_pgm(path: str):
	img: pgm = pgm()
	f = open(path, 'r')
	f.readline()
	f.readline()
	dims = f.readline()
	dims = dims.split(' ')
	img.x = int(dims[0])
	img.y = int(dims[1][0:-1])
	img.m = int(f.readline()[0:-1])
	img.data = []
	for i in range(img.x * img.y):
		img.data.append(int(f.readline()))
	return img


def crete_image(data: pgm):
	out_img = PIL.Image.new("RGB", (data.x, data.y))
	for y in range(data.y):
		for x in range(data.x):
			c: int = int(data.data[x + y * data.x] / data.m * 255)
			out_img.putpixel((x, y), (c, c, c))
	return out_img


def find_star(x: int, y: int, st: star, thershold: int, img: pgm):
	if x > 0 and y > 0 and x < img.x and y < img.y:
		if img.getpixel(x, y) > thershold:
			if not (x, y) in st.pixels:
				st.pixels.append((x, y))
				find_star(x-1, y, st, thershold, img)
				find_star(x+1, y, st, thershold, img)
				find_star(x, y-1, st, thershold, img)
				find_star(x, y+1, st, thershold, img)


def color_star(st: star, img: PIL.Image.Image):
	for p in st.pixels:
		img.putpixel(p, (255, 0, 0))


img = load_pgm("gwa.pgm")
out_img = crete_image(img)
for y in range(img.y):
	for x in range(0,img.x,2):
		if img.getpixel(x, y) > 150:
			minsize = 6
			st: star = star()
			find_star(x, y, st, 150, img)
			if img.getpixel(x, y) > 400:
				minsize = 2
			if len(st.pixels) > minsize:
				color_star(st, out_img)
out_img.save("by_test.bmp")
