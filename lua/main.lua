local function load_pgm()
    local pgm = {w = 0, h = 0, data = {}}
    local f = io.open("gwa.pgm", 'r')
    local file = f:read("a")
    local index = 0;
    index = file:find("\n", 0) + 1
    index = file:find("\n", index) + 1
    local index2 = file:find(" ", index) - 1
    pgm.w = tonumber(file:sub(index, index2))
    index = index2 + 2;
    index2 = file:find("\n", index) - 1
    pgm.h = tonumber(file:sub(index, index2))
    index = file:find("\n", index2) + 1
    index = file:find("\n", index) + 1

    for str in file:gmatch("[^\n]+", index) do
        table.insert(pgm.data, tonumber(str))
    end
    return pgm
end

local function create_out_img(pgm)
    local data_out = {}
    for index, value in ipairs(pgm.data) do
        local val = math.floor(value * 255 / 65535);
        table.insert(data_out, val)
        table.insert(data_out, val)
        table.insert(data_out, val)
    end
    return data_out
end
local function save_img(img)
    local out_file = io.open("lua_out.data", "w")
    for index, value in ipairs(img) do out_file:write(string.char(value)) end
end
local function getpixel(x, y, img) return img.data[img.w * y + x + 1] end
local function find_star(x, y, star, thereshold, img)
    if x > 0 and y > 0 and x < img.w and y < img.h then
        if getpixel(x, y, img) > thereshold then
            local isThere = false
            for index, value in ipairs(star.points) do
                if value.x == x and value.y == y then
                    isThere = true
                    break
                end
            end
            if not isThere then
                table.insert(star.points, {x = x, y = y})
                find_star(x - 1, y, star, thereshold, img)
                find_star(x + 1, y, star, thereshold, img)
                find_star(x, y - 1, star, thereshold, img)
                find_star(x, y + 1, star, thereshold, img)
            end
        end
    end

end
local function color_star(star, out_img, w)
    for index, value in ipairs(star.points) do
        out_img[(w * value.y + value.x) * 3 + 1] = 255;
        out_img[(w * value.y + value.x) * 3 + 2] = 0;
        out_img[(w * value.y + value.x) * 3 + 3] = 0;
        
    end
end
local pgm_img = load_pgm()
local out_img = create_out_img(pgm_img)

for y = 0, pgm_img.h-1,1 do
    for x = 0, pgm_img.w-1, 2 do
        if getpixel(x, y, pgm_img) > 150 then
            local star = {x = x, y = y, points = {}}
            find_star(x, y, star, 150, pgm_img)
            local star_size = 0;
            for index, value in ipairs(star.points) do
                star_size = star_size + 1
            end
            if getpixel(x, y, pgm_img) > 400 and star_size > 2 then
                color_star(star, out_img, pgm_img.w)
            elseif star_size > 6 then
                color_star(star, out_img, pgm_img.w)
            end
        end
    end
end
save_img(out_img)
