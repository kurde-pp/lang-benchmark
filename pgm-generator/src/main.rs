extern crate core;
extern crate rand;

use rand::{AsByteSliceMut, thread_rng};
use rand::Rng;
use std::env;
use std::fs::File;
use std::io::{stderr, Write};
use std::process::exit;

fn set_pixel(img: &mut Vec<u16>, x: u32, y: u32, size_x: u32, val: u16) {
    img[(x + y * size_x) as usize] = val;
}

fn generate_star(img: &mut Vec<u16>, x: i32, y: i32, val: i32, w: u32, h: u32) {
    if x < w as i32 && y < h as i32 && val > 0 && x >= 0 && y >= 0 {
        set_pixel(img, x as u32, y as u32, w, val as u16);
        let mut rng = rand::thread_rng();
        generate_star(img, x + 1, y, val + rng.gen_range(-8200, 60), w, h);
        generate_star(img, x - 1, y, val + rng.gen_range(-8200, 60), w, h);
        generate_star(img, x, y + 1, val + rng.gen_range(-8200, 60), w, h);
        generate_star(img, x, y - 1, val + rng.gen_range(-8200, 60), w, h);
    }
}

fn place_stars(img: &mut Vec<u16>, x: u32, y: u32, noises: u16) {
    let dots_number = x * y * noises as u32 / 65535;
    let mut rng = rand::thread_rng();
    for _ in 0..dots_number {
        generate_star(img, rng.gen_range(0, x) as i32 , rng.gen_range(0, y) as i32, rng.gen_range(200, 16000), x, y);
    }
}

fn generate_noise(img: &mut Vec<u16>, x: u32, y: u32, noises: u16) {
    let dots_number = x * y * noises as u32 / 65535;
    let mut rng = rand::thread_rng();
    for _ in 0..dots_number {
        set_pixel(img, rng.gen_range(0, x), rng.gen_range(0, y), x, rng.gen_range(0, 800));
    }
    for mut val in img.iter() {
        val = &rng.gen_range(0, 90);
    }
}

fn serialize_ppm(img: &mut Vec<u16>, x: u32, y: u32, path: &String) {
    let mut file = File::create(path).unwrap();
    file.write("P2\n# nikogo\n".as_bytes()).unwrap();
    file.write(x.to_string().as_bytes()).unwrap();
    file.write(" ".as_bytes()).unwrap();
    file.write(y.to_string().as_bytes()).unwrap();
    file.write("\n".as_bytes()).unwrap();
    file.write("65535\n".as_bytes()).unwrap();

    for val in img.iter() {
        file.write(val.to_string().as_bytes()).unwrap();
        file.write("\n".as_bytes()).unwrap();
    }
    file.flush().unwrap();
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 4 {
        stderr().write_all("Wrong args\n".as_bytes()).unwrap();
        exit(1);
    }
    let w_size: u32 = args[1].parse().unwrap();
    let h_size: u32 = args[2].parse().unwrap();
    let mut image = Vec::<u16>::with_capacity((h_size * w_size) as usize);
    image.resize((h_size * w_size) as usize, 0);
    generate_noise(&mut image, w_size, h_size, 40);
    place_stars(&mut image, w_size, h_size, 10);
    serialize_ppm(&mut image, w_size, h_size, &args[3]);
}
