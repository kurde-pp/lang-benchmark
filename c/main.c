#include <SDL2/SDL_stdinc.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
typedef struct {
  uint32_t size_x;
  uint32_t size_y;
  uint16_t *buffer;
} pgm_img;
typedef struct {
  uint32_t x;
  uint32_t y;

} pos;
typedef struct {
  pos coord;
  pos *points;
  uint32_t size;
  uint32_t capacity;

} star;
star create_star(uint32_t x, uint32_t y) {
  star st;
  st.coord.x = x;
  st.coord.y = y;
  st.points = malloc(sizeof(pos) * 6);
  st.capacity = 6;
  st.size = 0;
  return st;
}
void appeand_star(star *st, uint32_t x, uint32_t y) {
  if (st->capacity == st->size) {
    st->points = realloc(st->points, st->size * 2 * sizeof(pos));
    st->capacity = st->capacity * 2;
  }
  st->points[st->size].x = x;
  st->points[st->size].y = y;
  st->size++;
}
uint16_t get_pg_pixel(const pgm_img *pgm, uint32_t x, uint32_t y) {
  return pgm->buffer[pgm->size_x * y + x];
}
pgm_img load_pgm(const char *path) {
  FILE *f = fopen(path, "rb");
  fseek(f, 0, SEEK_END);
  uint32_t size = ftell(f);
  fseek(f, 0, SEEK_SET);
  char *filedata = malloc(size);
  fread(filedata, 1, size, f);
  uint32_t offset = 0;
  uint32_t line = 0;
  while (filedata[offset++] != '\n') {
  }
  while (filedata[offset++] != '\n') {
  }
  pgm_img pgm;
  pgm.size_x = atoi(filedata + offset);
  while (filedata[offset++] != ' ') {
  }
  pgm.size_y = atoi(filedata + offset);
  while (filedata[offset++] != '\n') {
  }
  pgm.buffer = malloc(pgm.size_y * pgm.size_x * sizeof(uint16_t));
  for (uint32_t y = 0; y < pgm.size_y; y++) {
    for (uint32_t x = 0; x < pgm.size_x; x++) {
      while (filedata[offset++] != '\n') {
      }
      pgm.buffer[pgm.size_x * y + x] = atoi(filedata + offset);
    }
  }
  free(filedata);
  return pgm;
}
unsigned char *export_data(const pgm_img *img) {
  unsigned char *out = malloc(3 * img->size_x * img->size_y);
  for (uint32_t i = 0; i < img->size_x * img->size_y; i++) {
    out[i * 3] = (uint32_t)img->buffer[i] * 255 / 65535;
    out[i * 3 + 1] = (uint32_t)img->buffer[i] * 255 / 65535;
    out[i * 3 + 2] = (uint32_t)img->buffer[i] * 255 / 65535;
  }
  return out;
}
uint8_t find_pos(const star *st, uint32_t x, uint32_t y) {
  for (uint32_t i = 0; i < st->size; i++) {
    if (x == st->points[i].x && y == st->points[i].y) {
      return 1;
    }
  }
  return 0;
}
void find_star(star *st, uint16_t thereshold, uint32_t x, uint32_t y,
               const pgm_img *img) {
  if (x < img->size_x && y < img->size_y) {
    if (get_pg_pixel(img, x, y) >= thereshold) {
      if (!find_pos(st, x, y)) {
        appeand_star(st, x, y);
        find_star(st, thereshold, x + 1, y, img);
        find_star(st, thereshold, x - 1, y, img);
        find_star(st, thereshold, x, y - 1, img);
        find_star(st, thereshold, x, y + 1, img);
      }
    }
  }
}
void color_pixel(const star *st, unsigned char *data, uint32_t size_x) {
  for (uint32_t i = 0; i < st->size; i++) {
    data[(st->points[i].y * size_x + st->points[i].x) * 3] = 0xff;
    data[(st->points[i].y * size_x + st->points[i].x) * 3 + 1] = 0x00;
    data[(st->points[i].y * size_x + st->points[i].x) * 3 + 2] = 0x00;
  }
}
int main(int argc, char **argv) {
  pgm_img img = load_pgm("gwa.pgm");
  unsigned char *out = export_data(&img);
  for (uint32_t y = 0; y < img.size_y; y++) {
    for (uint32_t x = 0; x < img.size_x; x += 2) {
      if (get_pg_pixel(&img, x, y) > 150) {
        star st = create_star(x, y);
        find_star(&st, 150, x, y, &img);
        if (get_pg_pixel(&img, x, y) > 400 && st.size > 2) {
          color_pixel(&st, out, img.size_x);
        } else if (st.size > 6) {
          color_pixel(&st, out, img.size_x);
        }
        free(st.points);
      }
    }
  }
  FILE *f = fopen("c_out.data", "wb");
  fwrite(out, 1, img.size_x * img.size_y * 3, f);
  fclose(f);
  free(out);
  free(img.buffer);
  return 0;
}