#!/bin/bash


TEST_PROG="hyperfine";
DCOMPILER="/usr/bin/gdc"
BENCH_FILE="gwa.pgm";
EXPOR_FILE="../RESULTS.md";
which cc &> /dev/null;
if [ $? -eq 1 ]
then 
	echo "C compiler not found";
	exit 1;
fi
which c++ &> /dev/null;
if [ $? -eq 1 ]
then 
	echo "C++ compiler not found";
	exit 1;
fi
which $DCOMPILER"" &> /dev/null;
if [ $? -eq 1 ]
then 
	echo "D compiler not found";
	exit 1;
fi
which zig &> /dev/null;
if [ $? -eq 1 ]
then 
	echo "zig compiler not found";
	exit 1;
fi
which cargo &> /dev/null;
if [ $? -eq 1 ]
then 
	echo "rust compiler not found";
	exit 1;
fi
which mono &> /dev/null;
if [ $? -eq 1 ]
then 
	echo "mono runtime not found";
	exit 1;
fi
which dotnet &> /dev/null;
if [ $? -eq 1 ]
then 
	echo "dotnet runtime not found";
	exit 1;
fi
which python3 &> /dev/null;
if [ $? -eq 1 ]
then 
	echo "python3 not found";
	exit 1;
fi

which ruby &> /dev/null;
if [ $? -eq 1 ]
then 
	echo "ruby not found";
	exit 1;
fi

which npm &> /dev/null;
if [ $? -eq 1 ]
then
	echo "npm not found";
	exit 1;
fi

which node &> /dev/null;
if [ $? -eq 1 ]
then
	echo "node not found";
	exit 1;
fi

which lua &> /dev/null;
if [ $? -eq 1 ]
then
	echo "lua not found";
	exit 1;
fi
which luajit &> /dev/null;
if [ $? -eq 1 ]
then
	echo "luajit not found";
	exit 1;
fi

rm build/out -rf;
mkdir -p build/out;

############### COMPILATION ###############

### Typescript -> Javascript
#Install tsc and other deps
cd js;
npm install &> buildlog.txt;
npm install typescript &> buildlog.txt;

#Run tsc (automatically runs main.js)
npx tsc &> buildlog.txt && rm buildlog.txt && echo  "＼(＾O＾)／ typescript converted to js";
mv  main.js  ../build/out/JsCode.js;
cp node_modules  ../build/out -r;
cp package-lock.json  ../build/out;
cp package.json  ../build/out;
cd ..;


### C
cc ./c/main.c -march=native  -O3 -o build/out/CCode && echo  "(｡◕‿‿◕｡) C program has been build";

### C++
c++ ./c++/main.cpp -march=native -O3 -DNDEBUG -o ./build/out/C++Code && echo "(◕‿◕✿) C++ version ready";

### C#
cd ./c\#/
dotnet build -c Release > buildlog.txt && echo "♥‿♥ C# program is here";
cp ./bin/Release/netcoreapp3.1/Stars.dll ../build/out/Stars.dll &&
mv ./bin/Release/netcoreapp3.1 ../build/out/CSharpCode &&
rm -rf bin &&
rm buildlog.txt &&
rm -rf obj;
cd .. ;

### Zig
cd zig;
zig build -Drelease-fast=true && echo "ಠ‿↼ Zig exec has been compiled!"; 
cp zig-out/bin/Gwiazdy ../build/out/ZigCode ;
rm -rf zig-out;
rm -rf zig-cache;
cd ..;

### D
$DCOMPILER ./dlang/main.d -O3 -march=native -o build/out/DCode && echo "(˵☯‿☯˵)つ Sucessfuly compiled D";

### Rust
cd rust;
cargo build --release &> buildlog.txt && rm buildlog.txt && echo "(✿╹◡╹)ﾉ☆.｡₀:*ﾟ Rust version works";
mv  target/release/lang-benchmark ../build/out/RustCode;
rm target -rf;
cd ..;


############### RUNNING ###############

echo;
echo;
echo "Compilers/Runtimes/Interpretrs versions";
echo $(c++ --version | head -n 1);
echo $(cc --version | head -n 1);
echo $(mono --version | head -n 1);
echo "zig "$(zig version | head -n 1);
echo $($DCOMPILER --version | head -n 1);
echo "dotnet "$(dotnet --version);
echo $(ruby --version | head -n 1);
echo $(python3 --version | head -n 1);
echo $(rustc --version | head -n 1);
echo "node "$(node --version);
cd build/out;
cp ../../gwa.pgm .;
$TEST_PROG  "./CCode" "./C++Code" "./RustCode" "./ZigCode"  "./DCode" "mono Stars.dll" "./CSharpCode/Stars" "node JsCode.js" "lua ../../lua/main.lua" "luajit ../../lua/main.lua" "python3 ../../python/main.py" "ruby ../../ruby/gwiazdy.rb"  --export-markdown=../RESULTS.md  --warmup 15; 
echo "Results saved to $RESULTS";
echo " (*◠‿◠*) Thanks to mesagungan,Aleksander, sppmacd and cub8! (ﾉ￫ܫ￩)ﾉ"

