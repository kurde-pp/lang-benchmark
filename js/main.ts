import * as fs from 'fs'
import * as readline from 'readline';
import * as bmp from 'bmp-js';

class Pgm {
	width: number = 0;
	height: number = 0;
	m: number = 65535;
	data: Uint16Array | null = null;

	getpixel(x: number, y: number) {
		return this.data![y * this.width + x];
	}
}

class Vec2 {
	x!: number;
	y!: number;
	constructor(x: number, y: number) {
		this.x = x;
		this.y = y;
	}
}

class Star {
	width: number = 0;
	height: number = 0;
	pixels = new Array<Vec2>();
}

function load_pgm(path: string) {
	let file_data = fs.readFileSync(path);

	let offset = 0;

	function readLine() {
		let start_offset = offset;
		while (true) {
			let ch = file_data[offset++];

			if (ch == 0x0A /* \n */ || offset >= file_data.length) {
				let sliced = file_data.slice(start_offset, offset - 1);
				return sliced.toString();
			}
		}
	}

	//P2
	readLine();

	//Comment
	readLine();

	let dims = readLine().split(' ');

	let image = new Pgm();
	image.width = parseInt(dims[0]);
	image.height = parseInt(dims[1]);

	console.log("width " + image.width + ", height " + image.height);

	image.m = parseInt(readLine());

	image.data = new Uint16Array(image.width * image.height);
	for (let y = 0; y < image.height; y++) {
		for (let x = 0; x < image.width; x++) {
			image.data[y * image.width + x] = parseInt(readLine());
		}
	}

	return image;
}

function find_star(x: number, y: number, st: Star, threshold: number, img: Pgm) {
	if (x < 0 || y < 0 || x >= img.width || y >= img.height)
		return;

	if (img.getpixel(x, y) <= threshold)
		return;


	for (let pixel of st.pixels) {
		if (pixel.x == x && pixel.y == y)
			return;//Already added
	}

	st.pixels.push(new Vec2(x, y));

	find_star(x - 1, y, st, threshold, img);
	find_star(x + 1, y, st, threshold, img);
	find_star(x, y - 1, st, threshold, img);
	find_star(x, y + 1, st, threshold, img);
}

let img = load_pgm("gwa.pgm");

let pitch = img.width * 4;
let out_img = Buffer.alloc(pitch * img.height);//Cleared by zeroes by default

function color_star(st: Star) {
	for (let p of st.pixels) {
		let offset = pitch * p.y + p.x * 4;
		out_img[offset + 3] = 255;//red
	}
}

for (let y = 0; y < img.height; y++) {
	for (let x = 0; x < img.width; x += 2) {
		let pxl = img.getpixel(x, y);
		if (pxl <= 150)
			continue;
		let minsize = 6;
		let st = new Star();
		find_star(x, y, st, 150, img);
		if (pxl > 400)
			minsize = 2;

		if (st.pixels.length > minsize) {
			color_star(st);
		}
	}
}

let bmp_data: any = {
	data: out_img,
	width: img.width,
	height: img.height
};

var raw_data = bmp.encode(bmp_data); //defaults to no compression
fs.writeFileSync('./js.bmp', raw_data.data);