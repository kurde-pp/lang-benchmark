class RGB
	attr_reader :r, :g, :b
	def initialize(r, g, b)
		@r = r
		@g = g
		@b = b
	end
end

class Pixel
	attr_reader :x, :y
	def initialize(x, y)
		@x = x
		@y = y
	end

	def ==(px)
		@x == px.x && @y == px.y
	end
end

class PGM
	attr_reader :x, :y
	attr_accessor :data
	def initialize(x, y, maxgray)
		@x = x
		@y = y
		@maxgray = maxgray
		@data = []
	end

	def get_pixel(pixel)
		raise TypeError, "Passed object is supposed to be a RGB, but is a #{pixel.class}" unless pixel.is_a? Pixel
		@data[pixel.x + @x * pixel.y]
	end
end

class PPM
	attr_reader :x, :y, :max_val
	attr_accessor :data
	def initialize(x, y)
		@x = x
		@y = y
		@max_val = 255
		@data = Array.new(x * y) {RGB.new(0, 0, 0)}
	end

	def put_pixel(pixel, rgb_val)
		raise TypeError, "Passed object is supposed to be a RGB, but is a #{pixel.class}" unless pixel.is_a? Pixel
		raise TypeError, "Passed object is supposed to be a RGB, but is a #{rgb_val.class}" unless rgb_val.is_a? RGB
		@data[pixel.x + @x * pixel.y] = rgb_val
	end
end

class Star
	attr_accessor :pixels
	def initialize()
		@pixels = [] # filled with Pixel class objects
	end
end

class StarsArcheologist
	def initialize(filepath_in)
		@filepath_in = filepath_in.to_s
		@filepath_out = @filepath_in.gsub("pgm", "ppm")
		@image = load_img()
		@save_image = PPM.new(@image.x, @image.y)
	end

	def run()
		y = 0
		x = 0
		while y < @image.y
			while x < @image.x
				current_pixel = Pixel.new(x, y)
				if @image.get_pixel(current_pixel) > 150
					if @image.get_pixel(current_pixel) > 400
						min_size = 2
					else
						min_size = 6
					end
					
					star = Star.new
					find_star(current_pixel, star, 150)

					if star.pixels.length > min_size
						color_star(star)
					end
				end
				x += 2
			end
			y += 1
			x = 0
		end

		write_img()
	end

private
	def load_img()
		file = File.new(@filepath_in, "r")
		file_data = file.readlines.map(&:chomp)
		file.close
		xy = file_data[2].split(' ').map(&:to_i)
		image = PGM.new(xy[0], xy[1], file_data[3].to_i)
		image.data = file_data.drop(4).map(&:to_i)
		image
	end

	def find_star(pixel, star, thershold)
		raise TypeError, "Passed object is supposed to be a Star, but is a #{pixel.class}" unless pixel.is_a? Pixel
		raise TypeError, "Passed object is supposed to be a Star, but is a #{star.class}" unless star.is_a? Star

		if pixel.x > 0 && pixel.y > 0 && pixel.x < @image.x && pixel.y < @image.y
			if @image.get_pixel(pixel) > thershold
				unless star.pixels.include? pixel
					star.pixels.push(pixel)
					find_star(Pixel.new(pixel.x - 1, pixel.y), star, thershold)
					find_star(Pixel.new(pixel.x + 1, pixel.y), star, thershold)
					find_star(Pixel.new(pixel.x, pixel.y + 1), star, thershold)
					find_star(Pixel.new(pixel.x, pixel.y - 1), star, thershold)
				end
			end
		end

	end

	def color_star(star)
		raise TypeError, "Passed object is supposed to be a Star, but is a #{star.class}" unless star.is_a? Star

		star.pixels.each do |pixel|
			@save_image.put_pixel(pixel, RGB.new(255, 0, 0))
		end
	end
	
	def write_img()
		file = File.new(@filepath_out, "w")
		file.write("P3\n")
		file.write("#{@save_image.x} #{@save_image.y}\n")
		file.write("#{@save_image.max_val}\n")
		@save_image.data.each do |rgb|
			file.write "#{rgb.r}\n"
			file.write "#{rgb.g}\n"
			file.write "#{rgb.b}\n"
		end
	end
end

archeologist = StarsArcheologist.new('gwa.pgm')
archeologist.run
