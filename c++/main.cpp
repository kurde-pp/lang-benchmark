#include "utils.hpp"
#include <algorithm>
#include <charconv>
#include <cstdlib>
#include <fstream>
#include <ios>
#include <iostream>
#include <stdio.h>
#include <string>
#include <string_view>
#include <vector>

struct star {
  u2_32 pos;
  std::vector<u2_32> pixels;
  star() { pixels.reserve(6); }
};
struct pgm_img {
  u32 w;
  u32 h;
  u16 max;
  std::vector<u16> data;
};
inline auto load_pgm(std::string path) {
  std::fstream file;
  file.open(path, std::ios::in);
  std::string content;
  file.seekg(0, std::ios::end);
  u32 size = file.tellg();

  file.seekg(0, std::ios::beg);
  content.resize(size);
  file.read((char *)content.data(), size);
  std::string_view str = content;
  auto index = str.find('\n');
  index = str.find('\n', index + 1) + 1;
  i32 w;
  i32 h;
  i32  max;
  std::from_chars(str.data() + index, str.data() + str.size() - index, w);
  index = str.find(" ", index) +1 ;
  std::from_chars(str.data() + index, str.data() + str.size() - index, h);
  pgm_img img;
  img.data.reserve(w * h);
  img.w = w;
  img.h = h;
  index = str.find('\n', index) + 1;
  std::from_chars(str.data() + index, str.data() + str.size() - index, max);
  index = str.find('\n', index) + 1;
  for (u32 i = 0; i < w * h; i++) {
    i32 tmp;
    std::from_chars(str.data() + index, str.data() + str.size() - index, tmp);
    img.data.push_back(tmp);
    index = str.find('\n', index) + 1;
  }
  return img;
}

void search_star(star &st, u16 thereshold, u2_32 pos, u2_32 data_size,
                 u16 *data) {
  if (pos.x < data_size.x && pos.y < data_size.y) {
    if (data[pos.x + pos.y * data_size.x] >= thereshold) {
      auto ret = std::find(st.pixels.begin(), st.pixels.end(), pos);
      if (ret == st.pixels.end()) {
        st.pixels.push_back(pos);
        search_star(st, thereshold, {pos.x + 1, pos.y}, data_size, data);
        search_star(st, thereshold, {pos.x, pos.y + 1}, data_size, data);
        search_star(st, thereshold, {pos.x - 1, pos.y}, data_size, data);
        search_star(st, thereshold, {pos.x, pos.y - 1}, data_size, data);
      }
    }
  }
}
inline auto copy_data(const pgm_img &img, std::vector<u8> &data) {

  for (u32 i = 0; i < img.w * img.h; i++) {
    data[i * 3] = (u32)img.data[i] * 255 / 65535;
    data[i * 3 + 1] = (u32)img.data[i] * 255 / 65535;
    data[i * 3 + 2] = (u32)img.data[i] * 255 / 65535;
  }
}
inline auto colorpixels(std::vector<u8> &surf, const std::vector<u2_32> &pos,
                        u32 sizex) {
  for (auto &p : pos) {
    surf[(p.y * sizex + p.x) * 3] = 0xff;
    surf[(p.y * sizex + p.x) * 3 + 1] = 0x00;
    surf[(p.y * sizex + p.x) * 3 + 2] = 0x00;
  }
}
int main() {
  pgm_img img;

  img = load_pgm("gwa.pgm");
  std::vector<u8> out_data(img.w * img.h * 3);
  copy_data(img, out_data);
  for (u32 y = 0; y < img.h; y++) {
    for (u32 x = 0; x < img.w; x += 2) {
      auto &luminace = img.data[x + y * img.w];
      if (luminace > 150) {
        star s;
        s.pos = {x, y};
        search_star(s, 150, s.pos, {(u32)img.w, (u32)img.h}, img.data.data());
        u32 min_size = 6;
        if (luminace > 400) {
          min_size = 2;
        }
        if (s.pixels.size() > min_size) {
          colorpixels(out_data, s.pixels, img.w);
        }
      }
    }
  }
  std::ofstream out_file("c++_out.data");
  out_file.write((char *)out_data.data(), out_data.size());
  out_file.close();
  return 0;
}