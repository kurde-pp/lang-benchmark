const std = @import("std");
const mem = std.mem;
const ArrayList = std.ArrayList;

const Image = struct {
    width: u32,
    height: u32,
    max: u32,
    pixels: []u32,

    fn getPixel(self: *const Image, x: u32, y: u32) u32 {
        return self.pixels[y * self.width + x];
    }

    fn readDecimal(stream: anytype) !u32 {
        // ignore whitespace
        var digit: u8 = undefined;
        while (true) {
            digit = try stream.readByte();
            if (!std.ascii.isSpace(digit))
                break;
        }
        var resultReversed: u32 = 0;
        while (true) {
            if (!std.ascii.isDigit(digit))
                break;
            resultReversed *= 10;
            resultReversed += (digit - '0');
            digit = try stream.readByte();
        }
        return resultReversed;
    }

    fn readPGM(filename: []const u8, allocator: mem.Allocator) !Image {
        var file = try std.fs.cwd().openFile(filename, .{});
        defer file.close();

        var stream = std.io.bufferedReader(file.reader()).reader();
        var buffer: [64]u8 = undefined;
        _ = (try stream.readUntilDelimiterOrEof(&buffer, '\n')) orelse return error.InvalidFormat; // P2
        _ = (try stream.readUntilDelimiterOrEof(&buffer, '\n')); // comment

        var width: u32 = try readDecimal(stream);
        var height: u32 = try readDecimal(stream);
        var max: u32 = try readDecimal(stream);

        var pixels = try allocator.alloc(u32, width * height);

        var i: u32 = 0;
        while (i < height) : (i += 1) {
            var j: u32 = 0;
            while (j < width) : (j += 1) {
                pixels[i * width + j] = try readDecimal(stream);
            }
        }

        return Image{ .width = width, .height = height, .max = max, .pixels = pixels };
    }

    fn deinit(self: Image, allocator: mem.Allocator) void {
        allocator.free(self.pixels);
    }
};

const Color = packed struct { r: u8, g: u8, b: u8 };

const OutputImage = struct {
    width: u32,
    height: u32,
    pixels: []Color,

    fn createEmpty(width: u32, height: u32, allocator: mem.Allocator) !OutputImage {
        return OutputImage{ .width = width, .height = height, .pixels = try allocator.alloc(Color, width * height) };
    }

    fn setPixel(self: OutputImage, x: u32, y: u32, pixel: Color) void {
        self.pixels[y * self.width + x] = pixel;
    }

    fn getPixel(self: OutputImage, x: u32, y: u32) Color {
        return self.pixels[y * self.width + x];
    }

    fn writeRaw(self: OutputImage, filename: []const u8) !void {
        var file = try std.fs.cwd().createFile(filename, .{});
        defer file.close();

        var stream = std.io.bufferedWriter(file.writer()).writer();
        for(self.pixels) |color| {
            try stream.writeStruct(color);
        }
    }

    fn deinit(self: OutputImage, allocator: mem.Allocator) void {
        allocator.free(self.pixels);
    }
};

const Vector = struct { x: u32, y: u32 };

const Star = struct {
    pixels: ArrayList(Vector) = undefined,

    fn init(allocator: mem.Allocator) Star {
        var star = Star{};
        star.pixels = ArrayList(Vector).init(allocator);
        return star;
    }

    fn deinit(star: Star) void {
        star.pixels.deinit();
    }
};

fn find_star(x: u32, y: u32, star: *Star, threshold: u32, img: *const Image, allocator: mem.Allocator) anyerror!void {
    if (x >= 1 and y >= 1 and x < img.width and y < img.height) {
        if (img.getPixel(x, y) > threshold) {
            const expected_pixel = Vector{ .x = x, .y = y };

            const item_index = for (star.pixels.items) |pixel, index| {
                if (pixel.x == expected_pixel.x and pixel.y == expected_pixel.y) break index;
            } else null;

            if (item_index == null) {
                try star.pixels.append(expected_pixel);
                try find_star(x - 1, y, star, threshold, img, allocator);
                try find_star(x + 1, y, star, threshold, img, allocator);
                try find_star(x, y - 1, star, threshold, img, allocator);
                try find_star(x, y + 1, star, threshold, img, allocator);
            }
        }
    }
}

fn color_star(star: *Star, output: *OutputImage) void {
    for (star.pixels.items) |pixel| {
        output.setPixel(pixel.x, pixel.y, Color{ .r = 255, .g = 0, .b = 0 });
    }
}

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();

    // FIXME: This should be taken from args
    const input = try Image.readPGM("./gwa.pgm", allocator);
    defer input.deinit(allocator);

    var output = try OutputImage.createEmpty(input.width, input.height, allocator);
    defer output.deinit(allocator);

    {
        var i: u32 = 0;
        while (i < input.height) : (i += 1) {
            var j: u32 = 0;
            while (j < input.width) : (j += 1) {
                const gsColor = @intCast(u8, input.getPixel(j, i) / 256);
                output.setPixel(j, i, Color{ .r = gsColor, .g = gsColor, .b = gsColor });
            }
        }
    }

    {
        var i: u32 = 0;
        var minSize: u32 = 6;
        while (i < input.height) : (i += 1) {
            var j: u32 = 0;
            while (j < input.width) : (j += 2) {
                const pixel = input.getPixel(j, i);
                if (pixel > 150) {
                    // FIXME: This probably could be done without allocations
                    minSize = 6;
                    var star = Star.init(allocator);
                    defer star.deinit();
                    try find_star(j, i, &star, 150, &input, allocator);
                    if (input.getPixel(j, i) > 400)
                        minSize = 2;
                    if (star.pixels.items.len > minSize)
                        color_star(&star, &output);
                }
            }
        }
    }

    try output.writeRaw("output.raw");
}
